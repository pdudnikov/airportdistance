# README #

REST service to measure distance in miles between two airports. Airports are identified by 3-letter IATA code. Uses third part service to get airport coordinates.
