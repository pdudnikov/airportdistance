﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AirportDistance.Module.Contract;
using Geolocation;

namespace AirportDistance.Module.Implementation
{
    internal class AirportDistanceProvider : IAirportDistanceProvider
    {
        private readonly IAirportCoordinatesProvider _airportCoordinatesProvider;

        public AirportDistanceProvider(IAirportCoordinatesProvider airportCoordinatesProvider)
        {
            _airportCoordinatesProvider = airportCoordinatesProvider;
        }

        public async Task<AirportDistanceModel> Provide(string airportCodeFrom, string airportCodeTo, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(airportCodeFrom))
            {
                throw new ArgumentException("AirportCodeFrom must not be empty");
            }

            if (string.IsNullOrWhiteSpace(airportCodeTo))
            {
                throw new ArgumentException("AirportCodeTo must not be empty");
            }

            var coordinatesFromTask = _airportCoordinatesProvider.Get(airportCodeFrom.ToUpperInvariant(), cancellationToken);
            var coordinatesToTask = _airportCoordinatesProvider.Get(airportCodeTo.ToUpperInvariant(), cancellationToken);

            await Task.WhenAll(coordinatesToTask, coordinatesFromTask);

            var coordinatesFrom = coordinatesFromTask.Result;
            var coordinatesTo = coordinatesToTask.Result;

            var distance = GeoCalculator.GetDistance(
                new Coordinate { Latitude = coordinatesFrom.Latitude, Longitude = coordinatesFrom.Longitude },
                new Coordinate { Latitude = coordinatesTo.Latitude, Longitude = coordinatesTo.Longitude },
                decimalPlaces: 1,
                distanceUnit: DistanceUnit.Miles);

            return new AirportDistanceModel(distance);
        }
    }
}
