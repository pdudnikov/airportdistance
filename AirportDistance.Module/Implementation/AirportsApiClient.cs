﻿using System;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using AirportDistance.Module.Contract;
using AirportDistance.Module.Contract.Exceptions;

namespace AirportDistance.Module.Implementation
{
    internal class AirportsApiClient : IAirportsApiClient
    {
        private readonly string _airportInfoUrl;
        private readonly HttpClient _httpClient;

        public AirportsApiClient(HttpClient httpClient, IOptions<AirportServicesConfiguration> configuration)
        {
            _httpClient = httpClient;
            _airportInfoUrl = configuration.Value.AirportInfoUrl!;
        }

        public async Task<AirportCoordinatesModel> Get(string airportCode, CancellationToken cancellationToken = default)
        {
            var response = await _httpClient.GetAsync($"{_airportInfoUrl}/{airportCode}", cancellationToken);
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                throw new AirportCodeNotFoundException($"Could not find airport with code '{airportCode}'");
            }

            if (!response.IsSuccessStatusCode && response.StatusCode != HttpStatusCode.BadRequest)
            {
                throw new WebException($"Airport info service returned unsuccessful result. Response code: {response.StatusCode}");
            }

            var airport = await JsonSerializer.DeserializeAsync<AirportInfoModel>(
                await response.Content.ReadAsStreamAsync(cancellationToken),
                cancellationToken: cancellationToken);

            if (airport?.Errors?.Any() == true)
            {
                throw new AirportCodeInvalidException(string.Join(Environment.NewLine, airport.Errors.Select(e => $"{e.Msg}: {e.Value}")));
            }

            if (airport?.Location == null)
            {
                throw new WebException("Airport info service returned is unexpected result.");
            }

            return new AirportCoordinatesModel(airport.Location!.Lon, airport.Location!.Lat);
        }
    }
}
