﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LazyCache;

namespace AirportDistance.Module.Implementation
{
    internal class AirportCoordinatesProvider : IAirportCoordinatesProvider
    {
        private readonly IAppCache _cache;
        private readonly IAirportsApiClient _airportsApiClient;

        public AirportCoordinatesProvider(IAirportsApiClient airportsApiClient, IAppCache cache)
        {
            _airportsApiClient = airportsApiClient;
            _cache = cache;
        }

        public async Task<AirportCoordinatesModel> Get(string airportCode, CancellationToken cancellationToken = default)
        {
            var coordinatesCache = await _cache.GetOrAddAsync(
                airportCode,
                () => GetCoordinates(airportCode, cancellationToken),
                DateTimeOffset.UtcNow.AddMinutes(10),
                ExpirationMode.ImmediateEviction);
            
            if (coordinatesCache.Value != null)
            {
                return coordinatesCache.Value;
            }

            throw coordinatesCache.Exception!;
        }

        private async Task<CoordinatesCache> GetCoordinates(string airportCode, CancellationToken cancellationToken)
        {
            try
            {
                var value = await _airportsApiClient.Get(airportCode, cancellationToken);
                return new CoordinatesCache(value, null);
            }
            catch (Exception e)
            {
                return new CoordinatesCache(null, e);
            }
        }

        internal sealed class CoordinatesCache
        {
            public CoordinatesCache(AirportCoordinatesModel? value, Exception? exception)
            {
                Value = value;
                Exception = exception;
            }

            public AirportCoordinatesModel? Value { get; }
            public Exception? Exception { get; }
        }
    }
}
