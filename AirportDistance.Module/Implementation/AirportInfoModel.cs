﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace AirportDistance.Module.Implementation
{
    [DataContract]
    internal class AirportInfoModel
    {
        [JsonPropertyName("country")]
        public string? Country { get; set; }

        [JsonPropertyName("city_iata")]
        public string? CityIata { get; set; }

        [JsonPropertyName("iata")]
        public string? Iata { get; set; }

        [JsonPropertyName("city")]
        public string? City { get; set; }

        [JsonPropertyName("timezone_region_name")]
        public string? TimezoneRegionName { get; set; }

        [JsonPropertyName("country_iata")]
        public string? CountryIata { get; set; }

        [JsonPropertyName("rating")]
        public int Rating { get; set; }

        [JsonPropertyName("name")]
        public string? Name { get; set; }

        [JsonPropertyName("location")]
        public LocationInfo? Location { get; set; }

        [JsonPropertyName("type")]
        public string? Type { get; set; }

        [JsonPropertyName("hubs")]
        public int Hubs { get; set; }

        [JsonPropertyName("errors")]
        public Error[]? Errors { get; set; }

        [DataContract]
        public class LocationInfo
        {
            [JsonPropertyName("lon")]
            public float Lon { get; set; }

            [JsonPropertyName("lat")]
            public float Lat { get; set; }
        }

        [DataContract]
        public class Error
        {
            [JsonPropertyName("value")]
            public string? Value { get; set; }

            [JsonPropertyName("msg")]
            public string? Msg { get; set; }

            [JsonPropertyName("param")]
            public string? Param { get; set; }

            [JsonPropertyName("location")]
            public string? Location { get; set; }
        }
    }

}
