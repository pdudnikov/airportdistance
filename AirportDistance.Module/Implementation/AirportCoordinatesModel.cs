﻿namespace AirportDistance.Module.Implementation
{
    internal sealed class AirportCoordinatesModel
    {
        public AirportCoordinatesModel(double longitude, double latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        public double Longitude { get; }
        public double Latitude { get; }
    }
}
