﻿using System.Threading;
using System.Threading.Tasks;

namespace AirportDistance.Module.Implementation
{
    internal interface IAirportsApiClient
    {
        Task<AirportCoordinatesModel> Get(string airportCode, CancellationToken cancellationToken = default);
    }
}
