﻿namespace AirportDistance.Module.Contract
{
    public sealed class AirportDistanceModel
    {
        public AirportDistanceModel(double distance)
        {
            Distance = distance;
        }

        public double Distance { get; }
    }
}
