﻿using System;

namespace AirportDistance.Module.Contract.Exceptions
{
    public sealed class AirportCodeNotFoundException : Exception
    {
        public AirportCodeNotFoundException(string msg) : base(msg)
        {
        }
    }
}
