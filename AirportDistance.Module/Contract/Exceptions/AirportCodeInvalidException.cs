﻿using System;

namespace AirportDistance.Module.Contract.Exceptions
{
    public sealed class AirportCodeInvalidException : Exception
    {
        public AirportCodeInvalidException(string msg) : base(msg)
        {
        }
    }
}
