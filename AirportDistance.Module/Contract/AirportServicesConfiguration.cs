﻿namespace AirportDistance.Module.Contract
{
    public sealed class AirportServicesConfiguration
    {
        public const string ConfigurationSection = "AirportServices";

        public string? AirportInfoUrl { get; set; }
    }
}
