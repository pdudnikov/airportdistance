﻿using System.Threading;
using System.Threading.Tasks;

namespace AirportDistance.Module.Contract
{
    public interface IAirportDistanceProvider
    {
        Task<AirportDistanceModel> Provide(string airportCodeFrom, string airportCodeTo, CancellationToken cancellationToken = default);
    }
}
