﻿using AirportDistance.Module.Contract;
using AirportDistance.Module.Implementation;
using LazyCache;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AirportDistance.Module
{
    public static class Dependencies
    {
        public static void Register(IServiceCollection serviceCollection, ConfigurationManager builderConfiguration)
        {
            serviceCollection.Configure<AirportServicesConfiguration>(
                builderConfiguration.GetSection(AirportServicesConfiguration.ConfigurationSection));
            serviceCollection.AddHttpClient<IAirportsApiClient, AirportsApiClient>();

            serviceCollection.AddSingleton<IAppCache, CachingService>();
            serviceCollection.AddTransient<IAirportsApiClient, AirportsApiClient>();
            serviceCollection.AddTransient<IAirportCoordinatesProvider, AirportCoordinatesProvider>();
            serviceCollection.AddTransient<IAirportDistanceProvider, AirportDistanceProvider>();
        }
    }
}
