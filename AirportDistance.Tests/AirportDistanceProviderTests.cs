﻿using AirportDistance.Module.Implementation;
using FakeItEasy;
using NUnit.Framework;

namespace AirportDistance.Tests
{
    [TestFixture]
    public sealed class AirportDistanceProviderTests
    {
        private IAirportCoordinatesProvider? _airportCoordinatesProviderFake;
        private AirportDistanceProvider? _provider;

        [SetUp]
        public void SetUp()
        {
            _airportCoordinatesProviderFake = A.Fake<IAirportCoordinatesProvider>();
            _provider = new AirportDistanceProvider(_airportCoordinatesProviderFake);
        }

        [TestCase(null, "cod")]
        [TestCase("", "cod")]
        [TestCase("   ", "cod")]
        [TestCase("cod", null)]
        [TestCase("cod", "")]
        [TestCase("cod", "   ")]
        public void Should_throw_exception_if_code_is_empty(string from, string to)
        {
            Assert.ThrowsAsync<ArgumentException>(() => _provider!.Provide(from, to));
        }

        [Test]
        public async Task Should_calculate_distance_between_locations()
        {
            var fromCode = "FRO";
            var toCode = "TOC";

            var fromLocation = new AirportCoordinatesModel(1, 1);
            var toLocation = new AirportCoordinatesModel(2, 2);

            var expectedResult = Math.Round(DistanceTo(
                fromLocation.Latitude,
                fromLocation.Longitude,
                toLocation.Latitude,
                toLocation.Longitude,
                'M'), 1);

            A.CallTo(() => _airportCoordinatesProviderFake!.Get(fromCode, A<CancellationToken>._)).Returns(fromLocation);
            A.CallTo(() => _airportCoordinatesProviderFake!.Get(toCode, A<CancellationToken>._)).Returns(toLocation);

            var result = await _provider!.Provide(fromCode, toCode);

            Assert.AreEqual(expectedResult, result.Distance);
        }

        // https://stackoverflow.com/questions/6366408/calculating-distance-between-two-latitude-and-longitude-geocoordinates
        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }
    }
}
