﻿using AirportDistance.Module.Implementation;
using FakeItEasy;
using LazyCache;
using LazyCache.Mocks;
using Microsoft.Extensions.Caching.Memory;
using NUnit.Framework;

namespace AirportDistance.Tests
{
    [TestFixture]
    public sealed class AirportCoordinatesProviderTests
    {
        private IAirportsApiClient? _airportsApiClientFake;
        private IAppCache? _appCacheFake;
        private AirportCoordinatesProvider? _provider;

        [SetUp]
        public void SetUp()
        {
            _airportsApiClientFake = A.Fake<IAirportsApiClient>();
            _appCacheFake = A.Fake<IAppCache>();
            _provider = new AirportCoordinatesProvider(_airportsApiClientFake, _appCacheFake);
        }

        [Test]
        public async Task Should_get_result_from_cache()
        {
            var code = "COD";
            var expectedResult = new AirportCoordinatesModel(1, 2);

            A.CallTo(() => _airportsApiClientFake!.Get(
                    code,
                    A<CancellationToken>._)).Returns(expectedResult);

            _provider = new AirportCoordinatesProvider(_airportsApiClientFake!, new MockCachingService());

            var result = await _provider!.Get(code);

            Assert.AreSame(expectedResult, result);
        }

        [Test]
        public async Task Should_get_result_from_api()
        {
            var code = "COD";
            var expectedResult = new AirportCoordinatesModel(1, 2);

            A.CallTo(() => _appCacheFake!.GetOrAddAsync(
                    code,
                    A<Func<ICacheEntry, Task<AirportCoordinatesProvider.CoordinatesCache>>>._,
                    A<MemoryCacheEntryOptions>._))
                .Returns(new AirportCoordinatesProvider.CoordinatesCache(expectedResult, null));

            var result = await _provider!.Get(code);

            Assert.AreSame(expectedResult, result);
        }

        [Test]
        public void Should_throw_exception_if_api_threw_exception()
        {
            var code = "COD";
            var expectedException = new Exception();

            A.CallTo(() => _airportsApiClientFake!.Get(
                code,
                A<CancellationToken>._)).Throws(expectedException);

            _provider = new AirportCoordinatesProvider(_airportsApiClientFake!, new MockCachingService());

            var resultException = Assert.ThrowsAsync<Exception>(() => _provider!.Get(code));

            Assert.AreSame(expectedException, resultException);
        }

        [Test]
        public void Should_throw_exception_if_cached_exception()
        {
            var code = "COD";
            var expectedException = new Exception();

            A.CallTo(() => _appCacheFake!.GetOrAddAsync(
                    code,
                    A<Func<ICacheEntry, Task<AirportCoordinatesProvider.CoordinatesCache>>>._,
                    A<MemoryCacheEntryOptions>._))
                .Returns(new AirportCoordinatesProvider.CoordinatesCache(null, expectedException));

            var resultException = Assert.ThrowsAsync<Exception>(() => _provider!.Get(code));

            Assert.AreSame(expectedException, resultException);
        }
    }
}
