using AirportDistance.Module.Contract;
using AirportDistance.Module.Contract.Exceptions;

var builder = WebApplication.CreateBuilder(args);

AirportDistance.Module.Dependencies.Register(builder.Services, builder.Configuration);

var app = builder.Build();

app.MapGet("/distance/{from}/{to}", async (IAirportDistanceProvider airportDistanceProvider, string from, string to, CancellationToken cancellationToken) =>
{
    try
    {
        var result = await airportDistanceProvider.Provide(from, to, cancellationToken);
        return Results.Ok(new { distance = result.Distance });
    }
    catch (AirportCodeInvalidException e)
    {
        return Results.BadRequest(new { e.Message });
    }
    catch (AirportCodeNotFoundException e)
    {
        return Results.NotFound(new { e.Message });
    }
});

app.Run();